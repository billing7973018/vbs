@echo off
echo Stopping services...
powershell -command "Stop-Service UmRdpService -Force"
powershell -command "Stop-Service TermService -Force"

echo Backing up termsrv.dll and changing permissions...
powershell -command "$termsrv_dll_acl = Get-Acl c:\windows\system32\termsrv.dll"
copy c:\windows\system32\termsrv.dll c:\windows\system32\termsrv.dll.copy
takeown /f c:\windows\system32\termsrv.dll
powershell -command "$new_termsrv_dll_owner = (Get-Acl c:\windows\system32\termsrv.dll).owner"
cmd /c "icacls c:\windows\system32\termsrv.dll /Grant %new_termsrv_dll_owner%:F /C"

echo Patching termsrv.dll...
powershell -command "$dll_as_bytes = Get-Content c:\windows\system32\termsrv.dll -Raw -Encoding byte"
powershell -command "$dll_as_text = $dll_as_bytes.forEach('ToString', 'X2') -join ' '"
powershell -command "$patternregex = ([regex]'39 81 3C 06 00 00(\s\S\S){6}')"
powershell -command "$patch = 'B8 00 01 00 00 89 81 38 06 00 00 90'"
powershell -command "$checkPattern=Select-String -Pattern $patternregex -InputObject $dll_as_text"

powershell -command "If ($checkPattern -ne $null) { $dll_as_text_replaced = $dll_as_text -replace $patternregex, $patch } Elseif (Select-String -Pattern $patch -InputObject $dll_as_text) { Write-Output 'The termsrv.dll file is already patched, exiting'; Exit } else { Write-Output 'Pattern not found ' }"

powershell -command "[byte[]] $dll_as_bytes_replaced = -split $dll_as_text_replaced -replace '^', '0x'"
powershell -command "Set-Content c:\windows\system32\termsrv.dll.patched -Encoding Byte -Value $dll_as_bytes_replaced"

echo Comparing patched file with original...
fc.exe /b c:\windows\system32\termsrv.dll.patched c:\windows\system32\termsrv.dll

echo Replacing original termsrv.dll with patched version...
copy c:\windows\system32\termsrv.dll.patched c:\windows\system32\termsrv.dll /y
powershell -command "Set-Acl c:\windows\system32\termsrv.dll $termsrv_dll_acl"

echo Starting services...
powershell -command "Start-Service UmRdpService"
powershell -command "Start-Service TermService"

echo Done!
